package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/sirupsen/logrus"
	"nononsensecode.com/pgx-mocking/cmd/types"
	"nononsensecode.com/pgx-mocking/pkg/domain/model/user"
	"nononsensecode.com/pgx-mocking/pkg/infrastructure/sql/pgsql"
)

var (
	repo user.Repository
)

func main() {
	p := PgSqlPoolProvider{
		host:     "127.0.0.1",
		port:     5432,
		user:     "kaushik",
		password: "redhat",
		db:       "users",
	}
	repo = pgsql.New(p)

	http.HandleFunc("/users", Save)

	fmt.Println("starting server")
	http.ListenAndServe(":8000", nil)
}

func Save(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-Type", "application/json")

	if r.Method != "POST" {
		w.WriteHeader(http.StatusMethodNotAllowed)
		e := ErrorResponse{Msg: "method not allowed"}
		json.NewEncoder(w).Encode(e)
		return
	}

	var u UserDto
	if err := json.NewDecoder(r.Body).Decode(&u); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		e := ErrorResponse{Msg: "invalid data"}
		json.NewEncoder(w).Encode(e)
		return
	}

	saved, err := repo.Save(r.Context(), u.Unmarshal())
	if err != nil {
		logrus.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		e := ErrorResponse{Msg: "internal server error"}
		json.NewEncoder(w).Encode(e)
		return
	}

	w.WriteHeader(http.StatusCreated)
	id := UserId{Id: saved.ID()}
	json.NewEncoder(w).Encode(id)
}

type UserId struct {
	Id int64 `json:"id"`
}

type UserDto struct {
	Username  string `json:"username"`
	FirstName string `json:"firstName"`
	LastName  string `json:"lastName"`
	Email     string `json:"email"`
	Password  string `json:"password"`
}

func (u *UserDto) Unmarshal() user.User {
	return user.New(u.Username, u.FirstName, u.LastName, u.Email, u.Password)
}

type ErrorResponse struct {
	Msg string `json:"msg"`
}

type PgSqlPoolProvider struct {
	host     string
	port     int
	user     string
	password string
	db       string
}

func (pp PgSqlPoolProvider) GetPool(ctx context.Context) (p types.PgxIface, err error) {
	connString := fmt.Sprintf("postgres://%s:%s@%s:%d/%s", pp.user, pp.password, pp.host, pp.port, pp.db)
	return pgxpool.Connect(ctx, connString)
}
