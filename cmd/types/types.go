package types

import (
	"context"

	"github.com/jackc/pgx/v4"
)

type PgSqlPoolProvider interface {
	GetPool(context.Context) (PgxIface, error)
}

type PgxIface interface {
	Begin(context.Context) (pgx.Tx, error)
	Close()
}
