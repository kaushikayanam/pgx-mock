package pgsql

import (
	"context"
	"fmt"
	"testing"

	"github.com/pashagolub/pgxmock"
	"github.com/stretchr/testify/assert"
	"nononsensecode.com/pgx-mocking/cmd/types"
	"nononsensecode.com/pgx-mocking/pkg/domain/model/user"
)

type mockProvider struct {
	pool pgxmock.PgxPoolIface
}

func (mp mockProvider) GetPool(ctx context.Context) (p types.PgxIface, err error) {
	return mp.pool, nil
}

func Test_Save(t *testing.T) {
	pool, err := pgxmock.NewPool()
	if err != nil {
		t.Errorf("creating mock pool should not return errors")
		return
	}
	defer pool.Close()

	mp := mockProvider{pool: pool}
	repo := New(mp)

	pool.ExpectBegin().WillReturnError(fmt.Errorf("transaction cannot be started"))

	u := user.New("kaushik", "kaushik", "asokan", "kaushi@gmail.com", "abcdefg")
	_, err = repo.Save(context.Background(), u)
	if err == nil {
		t.Errorf("as tx cannot be started, save should return error")
		return
	}

	if assert.Contains(t, err.Error(), "tx cannot be started") {
		return
	}
}
