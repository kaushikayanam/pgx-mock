package pgsql

import (
	"context"
	"fmt"

	"github.com/jackc/pgx/v4"
	"go.uber.org/multierr"
	"nononsensecode.com/pgx-mocking/cmd/types"
	"nononsensecode.com/pgx-mocking/pkg/domain/model/user"
)

type PgSqlUserRepository struct {
	provider types.PgSqlPoolProvider
}

func New(p types.PgSqlPoolProvider) PgSqlUserRepository {
	if p == nil {
		panic("provider is nil")
	}

	return PgSqlUserRepository{p}
}

func (p PgSqlUserRepository) Save(ctx context.Context, u user.User) (saved user.User, err error) {
	var (
		pool types.PgxIface
		tx   pgx.Tx
		id   int64
	)
	pool, err = p.provider.GetPool(ctx)
	if err != nil {
		return
	}
	defer func() {
		pool.Close()
	}()

	tx, err = pool.Begin(ctx)
	if err != nil {
		err = fmt.Errorf("tx cannot be started: %w", err)
		return
	}

	err = tx.QueryRow(ctx, "INSERT INTO users (user_name, first_name, last_name, email, password) VALUES ($1, $2, $3, $4, $5) RETURNING id",
		u.Username(), u.FirstName(), u.LastName(), u.Email(), u.Password()).Scan(&id)
	if err != nil {
		return
	}
	defer func() {
		err = FinishTx(ctx, tx, err)
	}()

	saved = user.Umarshal(id, u.Username(), u.FirstName(), u.LastName(), u.Email(), u.Password())
	return
}

func FinishTx(ctx context.Context, tx pgx.Tx, err error) error {
	if err != nil {
		if rErr := tx.Rollback(ctx); rErr != nil {
			rErr = fmt.Errorf("rollback failed: %w", rErr)
			return multierr.Append(rErr, err)
		}
		return err
	}

	if cErr := tx.Commit(ctx); cErr != nil {
		cErr = fmt.Errorf("commit failed: %w", cErr)
		return cErr
	}
	return err
}
