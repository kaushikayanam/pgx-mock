package user

type User struct {
	id        int64
	username  string
	firstName string
	lastName  string
	email     string
	password  string
}

func New(username, firstName, lastName, email, password string) User {
	return User{
		username:  username,
		firstName: firstName,
		lastName:  lastName,
		email:     email,
		password:  password,
	}
}

func Umarshal(id int64, username, firstName, lastName, email, password string) User {
	u := New(username, firstName, lastName, email, password)
	u.id = id
	return u
}

func (u User) ID() int64 {
	return u.id
}

func (u User) Username() string {
	return u.username
}

func (u User) FirstName() string {
	return u.firstName
}

func (u User) LastName() string {
	return u.lastName
}

func (u User) Email() string {
	return u.email
}

func (u User) Password() string {
	return u.password
}
